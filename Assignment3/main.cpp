/*******************************************************************************
main.cpp

By Isaiah Peters
ENEL 487 - Assignment 3
November 30, 2017

This code generates a random password consisting of five dictionary words

Tab size = 4
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "dict_impl.h"

unsigned getSeedFromUser ();

void generatePasswd ();

int main ()
{
	unsigned seed;
	
    seed = getSeedFromUser ();
    
    srand (seed);
    
	while (1)
    {
        generatePasswd ();
    }
	
}

unsigned getSeedFromUser ()
{
    unsigned userInput;
    
	printf ("Please enter a random 5-digit number: ");
    scanf ("%u", &userInput);
    
    while (!((userInput >= 10000) && (userInput <= 99999)))
    {
        printf ("That was not a valid 5-digit number.  Please try again: ");
        scanf ("%u", &userInput);
    }
    
    return userInput;
}

void generatePasswd ()
{
    int offset;
    int garbage;
    
    printf ("Enter a random 2-digit number to generate a password: ");
    
    scanf ("%u", &offset);
    
    while (!((offset >= 10) && (offset <= 99)))
    {
        printf ("That was not a valid 2-digit number.  Please try again: ");
        scanf ("%u", &offset);
    }
    
    printf ("Your password is:\n\n");
    
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < offset; j++)
        {
            garbage = rand ();
        }
        
        printNextWord ((((rand () * 27u) + (rand () % 27)) % LAST_WORD_START));
        
        if (i != 4)
            printf (" ");
    }
    
    printf ("\n\n");
}
