/*******************************************************************************
dict_impl.h

By Isaiah Peters
ENEL 487 - Assignment 3
November 30, 2017

This file contains the function definitions for dictionary processing.

Tab size = 4
*******************************************************************************/

#define LAST_WORD_START       643220u

void printNextWord (unsigned);
