ENEL 487 - Assignment 3
Isaiah Peters (200337734)
December 1, 2017


-- BASIC INFORMATION --
This C++ program generates high-entropy passwords.  Each password consists of
five randomly selected words from a preprocessed english dictionary.


-- PREPROCESSING --
The first step in preprocessing the dictionary file was to remove posessive
words.  This was done with a single line on Snoopy:

grep -v ".*'s$" american-english > no-possessive-words

Next, word with unicode characters were removed also using a single line on
Snoopy:

grep -P -v '[^\x00-\x7f]' american-english > no-unicode-words

The remaining automated processing was compleded with the use of a Python
script "Python Processing.py".  This script went through the processed
dictionary on letter at a time and enclosed each character in single quotes and
added a comma and space afterwards.  The output was sent to a .h file.  The
script also added the syntax to the beginning of the file to declare the
array.  The number of elements in the array was printed at the end of the
file and was then copied to the top and reformatted.  A closing brace and 
semicolon were added manually.


-- COMPILING --
To run the program on Snoopy, simply type "make" and then ./assign3