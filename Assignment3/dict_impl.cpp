/*******************************************************************************
dict_impl.cpp

By Isaiah Peters
ENEL 487 - Assignment 3
November 30, 2017

This file contains the function declaration for dictionary processing.

Tab size = 4
*******************************************************************************/

#include <stdio.h>

#include "dictionary.h"
#include "dict_impl.h"

void printNextWord (unsigned id)
{
    // Check that the id is in range
    if (id > LAST_WORD_START)
        return;
    
    // Start in a random location in the dictionary
    // Move forward until '/n' is found
    // Move forward one more to the first letter of the next word
    while (dictionary[id] != '\n')
        id ++;
    id++;
    
    while (dictionary[id] != '\n')
    {
        printf ("%c", dictionary[id]);
        id++;
    }
}
