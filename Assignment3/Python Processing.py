inFile = 'american-english'
outFile = 'output.h'

count = 0

with open(outFile, 'w') as fout:
    with open(inFile) as fin:  
        fout.write('const char dictionary[] = {')
        line = fin.readline()
        while line:
            if len(line) > 5:
                for letter in line[:-1]:
                    if letter != '\'':
                        count += 1
                        fout.write('\'')
                        fout.write(letter)
                        fout.write('\', ')
                fout.write('\'\\n\',\n')
                count += 1
            line = fin.readline()
    fout.write ('count = ')
    fout.write (str(count))
	
fin.close
fout.close
