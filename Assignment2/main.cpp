/*******************************************************************************
main.cpp

By Isaiah Peters
ENEL 487 - Assignment 2
October 4, 2017

This file contains the general code for performing operation with complex
numbers.  It uses complex.h and complex.cpp.

Tab size = 4
*******************************************************************************/
//<-------------------------- 80 Character Ruler ----------------------------->|

#include <iostream>
#include <stdio.h>
#include <sstream>
#include <string>

using namespace std;

#include "complex.h"

//=============================== Operation Enum ===============================
enum Operation {ERROR = 0, ADD = 1, SUBTRACT = 2, MULTIPLY = 3, DIVIDE = 4,
    ABS = 5, ARG = 6, ARGDEG = 7, EXP = 8, INV = 9, QUIT = 10};



//============================ Function Declarations ===========================
/**
    welcomeMessage - Welcome Message

    This function prints the name of the program as well as basic instructions.
*/
void welcomeMessage (void);


/**
    getExpression - Get Expression

    This function prompts the user to enter a command and then collects the
    user's input.  The selected operation is returned (as an operation using the
    enum described at the beginning of main.cpp) and the values of the one or
    two complex numbers are passed by reference.
*/
Operation getExpression (Complex &cmplx1, Complex &cmplx2);


/**
    handleExpression - Handle Expression

    This uses "operation" variable to determine how to handle the user's
    expression.  If appropriate, it calls "complexResultMath" or
    "simpleResultMath" to perform a calculation.
*/
void handleExpression (Operation op, Complex cmplx1, Complex cmplx2);


/**
    complexResultMath - Complex Result Math

    This function takes two Complex numbers and an Operation as inputs and
    performs some complex math operation.  The Operation contains the
    instruction for the type of operation to perform.  The result is returned
    as a Complex number.
*/
Complex complexResultMath (Operation op, Complex cmplx1, Complex cmplx2);


/**
    simpleResultMath - Simple Result Math

    This function takes one Complex number and an operation as inputs and
    performs some complex math operation.  The short contains the instruction
    for the type of operation to perform.  The result is returned as a
    Complex number.
*/
double simpleResultMath (Operation op, Complex cmplx);


/**
    checkDivZero - Check for Division by Zero
    
    This function takes in an Operation and two Complex numbers and returns
    true if the operation will involve dividing by zero or false otherwise.
*/
bool checkDivZero (Operation op, Complex cmplx1, Complex cmplx2);


/**
    convertToLowercase - Convert to Lowercase

    This function converts all characters in a string to lowercase.
*/
string convertToLowercase (string str);


/**
    parseOperation - Parse Operation

    This function takes in a string, finds a matching operation, and returns 
    the corrosponding Operation.  This function utilizes the convertToLowercase
    function.
*/
Operation parseOperation (string opStr);





//=============================== Main Function ================================
int main ()
{
    Operation op = ERROR;
    Complex cmplx1, cmplx2;

    welcomeMessage ();

    while (op != QUIT)
    {
        op = getExpression (cmplx1, cmplx2);

        handleExpression (op, cmplx1, cmplx2);
    }

    return 0;
}



//============================ Function Definitions ============================
Operation getExpression (Complex &cmplx1, Complex &cmplx2)
{
    Operation op;
    string inputStr;
    string operation;
    stringstream ss;

    cmplx1.real = 0;
    cmplx1.imag = 0;
    cmplx2.real = 0;
    cmplx2.imag = 0;

    cerr << "Enter exp: ";
    getline (cin, inputStr);
    ss.str (inputStr);

    ss >> operation >> cmplx1.real >> cmplx1.imag >> cmplx2.real >> cmplx2.imag;

    op = parseOperation (operation);

    if (op == ERROR)
    {
        cerr << "Malformed command: input line was: " << inputStr << endl;
    }

    else if (checkDivZero (op, cmplx1, cmplx2))
    {
        op = ERROR;
        cerr << "Division by 0: input line was: " << inputStr << endl;
    }

    return op;
}


void welcomeMessage (void)
{
    cerr << "Complex Calculator\n\n";
    cerr << " Type a letter to specify the arithmetic operator (A, S, M, D)\n"
        << " followed by two complex numbers expressed as pairs of "
        << "doubles.\n Type a command (ABS, ARG, ARGDEG, EXP, INV) followed"
        << " by one \n complex number expressed as a pair of doubles.\n"
        << " Type Q to quit.\n\n";

    return;
}


Complex complexResultMath (Operation op, Complex cmplx1, Complex cmplx2)
{
    if (op == ADD)
    {
        return cmplxAdd (cmplx1, cmplx2);
    }

    else if (op == SUBTRACT)
    {
        return cmplxSubtract (cmplx1, cmplx2);
    }

    else if (op == MULTIPLY)
    {
        return cmplxMultiply (cmplx1, cmplx2);
    }

    else if (op == DIVIDE)
    {
        return cmplxDivide (cmplx1, cmplx2);
    }

    else if (op == EXP)
    {
        return cmplxExp (cmplx1);
    }

    else if (op == INV)
    {
        return cmplxInv (cmplx1);
    }

    else
    {
        Complex error;
        error.real = 0;
        error.imag = 0;

        return error;
    }
}


double simpleResultMath (Operation op, Complex cmplx)
{
    if (op == ABS)
    {
        return cmplxAbs (cmplx);
    }

    else if (op == ARG)
    {
        return cmplxArg (cmplx);
    }

    else if (op == ARGDEG)
    {
        return cmplxArgDeg (cmplx);
    }

    else
    {
        return 0.0;
    }
}


void handleExpression (Operation op, Complex cmplx1, Complex cmplx2)
{
    if ((op != ERROR) && (op != QUIT))
    {
        if ((op == ADD) || (op == SUBTRACT) || (op == MULTIPLY)
            || (op == DIVIDE) || (op == EXP) || (op == INV))
        {
            cmplxPrint (complexResultMath(op, cmplx1, cmplx2));
        }

        else
        {
            cout << simpleResultMath (op, cmplx1) << endl;
        }
    }

    return;
}


bool checkDivZero (Operation op, Complex cmplx1, Complex cmplx2)
{
    if (op == DIVIDE)
    {
        if ((cmplx2.real == 0) && (cmplx2.imag == 0))
        {
            return true;
        }
    }

    else if ((op == ARG) || (op == ARGDEG) || (op == INV))
    {
        if ((cmplx1.real == 0) && (cmplx1.imag == 0))
        {
            return true;
        }
    }

    return false;
}


string convertToLowercase (string str)
{
    for (unsigned int i = 0; i < str.length(); i++)
    {
        str[i] = tolower(str[i]);
    }

    return str;
}


Operation parseOperation (string opStr)
{
    Operation op;
    string lowercaseOpStr;

    lowercaseOpStr = convertToLowercase (opStr);

    if (lowercaseOpStr.compare ("a") == 0)
    {
        op = ADD;
    }

    else if (lowercaseOpStr.compare ("s") == 0)
    {
        op = SUBTRACT;
    }

    else if (lowercaseOpStr.compare ("m") == 0)
    {
        op = MULTIPLY;
    }

    else if (lowercaseOpStr.compare ("d") == 0)
    {
        op = DIVIDE;
    }

    else if (lowercaseOpStr.compare ("abs") == 0)
    {
        op = ABS;
    }

    else if (lowercaseOpStr.compare ("arg") == 0)
    {
        op = ARG;
    }

    else if (lowercaseOpStr.compare ("argdeg") == 0)
    {
        op = ARGDEG;
    }

    else if (lowercaseOpStr.compare ("exp") == 0)
    {
        op = EXP;
    }

    else if (lowercaseOpStr.compare ("inv") == 0)
    {
        op = INV;
    }

    else if (lowercaseOpStr.compare ("q") == 0)
    {
        op = QUIT;
    }

    else
    {
        op = ERROR;
    }

    return op;
}