ENEL 487 - Assignment 2
Isaiah Peters (200337734)
October 4, 2017

-- BASIC INFORMATION --
This C++ program uses three files to demonstrate a datatype (with operations)
for complex numbers.  The nine permitted operations are:
 - Addition (A)
 - Subtraction (D)
 - Multiplication (M)
 - Division (D)
 - Absolute/Magnitude (ABS)
 - Argument/Angle in radians (ARG)
 - Argument/Angle in degrees (ARGDEG)
 - Exponent (EXP)
 - Inverse (INV)


The complex number datatype is made up of a struct that has two doubles: one for
the real component of the number, and one for the imaginary component.  The math
operations use the simple C++ operators (e.g. +,-,*,/) and some functions from
the cmath library (exp(), sin(), cos(), atan()) in complex math formulas to
perform complex arithmetic.

All the code for the complex datatype is stored in the files complex.h and
complex.cpp.

A main.cpp file was written that utilizes the complex datatype operations
and performs complex math.  The main.cpp file takes an expression command
(listed in brackets after each operation in the list above), and either one or
two pairs of doubles as the complex number(s).  An invalid command will return
an error message and that includes the original command typed by the user.  The
program will also detect if an operation entered by the user will cause a divide
by zero error and will return an error message.


-- BUILD INSTRUCTIONS --
Simply run the 'make' command in the directory with complex.h, complex.cpp, and
main.cpp and the program will be compiled with g++.
