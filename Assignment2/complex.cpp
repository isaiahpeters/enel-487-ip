/*******************************************************************************
complex.cpp

By Isaiah Peters
ENEL 487 - Assignment 2
October 4, 2017

This file contains the function definitions for the "Complex" datatype.

Tab size = 4
*******************************************************************************/
//<-------------------------- 80 Character Ruler ----------------------------->|

#include <iostream>
#include <cmath>

using namespace std;

#include "complex.h"

#define PI 3.1415926535897932384626433832795

Complex cmplxAdd (Complex addend1, Complex addend2)
{
    Complex sum;

    sum.real = addend1.real + addend2.real;
    sum.imag = addend1.imag + addend2.imag;

    return sum;
}

Complex cmplxSubtract (Complex minuend, Complex subtrahend)
{
    Complex difference;

    difference.real = minuend.real - subtrahend.real;
    difference.imag = minuend.imag - subtrahend.imag;

    return difference;
}

Complex cmplxMultiply (Complex factor1, Complex factor2)
{
    Complex product;

    product.real = (factor1.real * factor2.real)
        - (factor1.imag * factor2.imag);

    product.imag = (factor1.imag * factor2.real)
        + (factor1.real * factor2.imag);

    return product;
}

Complex cmplxDivide (Complex dividend, Complex divisor)
{
    Complex quotient;

    quotient.real = ((dividend.real * divisor.real)
        + (dividend.imag * divisor.imag)) / ((divisor.real * divisor.real)
        + (divisor.imag * divisor.imag));

    quotient.imag = ((dividend.imag * divisor.real)
        - (dividend.real * divisor.imag)) / ((divisor.real * divisor.real)
        + (divisor.imag * divisor.imag));

    return quotient;
}

double cmplxAbs (Complex num)
{
    double result;

    result = sqrt ((num.real * num.real) + (num.imag * num.imag));

    return result;
}

double cmplxArg (Complex num)
{
    double result;

    result = atan2 (num.imag, num.real);

    return result;
}

double cmplxArgDeg (Complex num)
{
    double result;

    result = (atan2 (num.imag, num.real)) * 180 / PI;

    return result;
}

Complex cmplxExp (Complex num)
{
    Complex result;

    result.real = exp(num.real) * cos(num.imag);
    result.imag = exp(num.real) * sin(num.imag);

    return result;
}

Complex cmplxInv (Complex num)
{
    Complex result;

    result.real = num.real / ((num.real * num.real) + (num.imag * num.imag));
    result.imag = -1.0 * num.imag / ((num.real * num.real)
        + (num.imag * num.imag));

    return result;
}

void cmplxPrint (Complex number)
{
    cout << number.real;

    if (number.imag >= 0)
        cout << " + j " << number.imag;

    else
        cout << " - j " << (number.imag * -1);

    cout << endl;
}