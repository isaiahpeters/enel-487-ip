/*******************************************************************************
complex.h

By Isaiah Peters
ENEL 487 - Assignment 1
September 17, 2017

This file contains the function declaration for the "Complex" datatype.

Tab size = 4
*******************************************************************************/
//<-------------------------- 80 Character Ruler ----------------------------->|

#include <iostream>

using namespace std;

/**
Complex struct
Stores complex numbers

double real => stores the real component of the complex number
double imaginary => stores the imaginary component of the complex number
*/

struct Complex
{
    double real;
    double imag;
};


Complex cmplxAdd (Complex addend1, Complex addend2);

Complex cmplxSubtract (Complex minuend, Complex subtrahend);

Complex cmplxMultiply (Complex factor1, Complex factor2);

Complex cmplxDivide (Complex dividend, Complex divisor);

void cmplxPrint (Complex number);