ENEL 487 - Assignment 1
Isaiah Peters (200337734)
September 17, 2017

-- BASIC INFORMATION --
This C++ program uses three files to demonstrate a datatype (with operations)
for complex numbers.  The simple operations allowed are for addition,
subtraction, multiplication, division, and printing a number to the terminal.

The complex number datatype is made up of a struct that has two doubles: one for
the real component of the number, and one for the imaginary component.  The
arithmetic operations use the simple C++ operators (e.g. +,-,*,/) in complex
math formulas to perform complex arithmetic.

All the code for the complex datatype is stored in the files complex.h and
complex.cpp.

A simple main.cpp file was written that utilizes the complex datatype operations
and performs simple complex math.  The main.cpp file takes a letter as a
command, two doubles for the first complex number, and two more doubles for the
second complex number.  An invalid command letter will return an error message.


-- BUILD INSTRUCTIONS --
Simply run the 'make' command in the directory with complex.h, complex.cpp, and
main.cpp and the program will be compiled with g++.
