/*******************************************************************************
main.cpp

By Isaiah Peters
ENEL 487 - Assignment 1
September 17, 2017

This file contains the general code for performing operation with complex
numbers.  It uses complex.h and complex.cpp.

Tab size = 4
*******************************************************************************/
//<-------------------------- 80 Character Ruler ----------------------------->|

#include <iostream>
#include <stdio.h>

using namespace std;

#include "complex.h"

//================================ Constants ===================================
const short ERROR = 0;
const short ADD = 1;
const short SUBTRACT = 2;
const short MULTIPLY = 3;
const short DIVIDE = 4;
const short QUIT = 5;



//============================ Function Declarations ===========================
/**
    welcomeMessage - Welcome Message

    This function prints the name of the program as well as basic instructions.
*/
void welcomeMessage (void);

/**
    getExpression - Get Expression

    This function prompts the user to enter a command and then collects the
    user's input.  The selected operation is returned (as a short using the
    constants described at the beginning of main.cpp) and the values of the two
    complex numbers are passed by reference.
*/
short getExpression (Complex &cmplx1, Complex &cmplx2);

/**
    handleExpression - Handle Expression

    This uses "operation" variable to determine how to handle the user's
    expression.  If appropriate, it calls "cmplxMath" to perform a calculation.
*/
void handleExpression (short operation, Complex cmplx1, Complex cmplx2);

/**
    cmplxMath - Complex Math

    This function takes two Complex numbers and a short as inputs and
    performs some complex math operation.  The short contains the instruction
    for the type of operation to perform.  The result is returned as a
    Complex number.
*/
Complex cmplxMath (short operation, Complex cmplx1, Complex cmplx2);



//=============================== Main Function ================================
int main ()
{
    short operation = 0;
    Complex cmplx1, cmplx2;

    welcomeMessage ();

    while (operation != QUIT)
    {
        operation = getExpression (cmplx1, cmplx2);

        handleExpression (operation, cmplx1, cmplx2);
    }

    return 0;
}



//============================ Function Definitions ============================
short getExpression (Complex &cmplx1, Complex &cmplx2)
{
    short operation;
    char opChar;

    cmplx1.real = 0;
    cmplx1.imag = 0;
    cmplx2.real = 0;
    cmplx2.imag = 0;

    cerr << "Enter exp: ";
    cin >> opChar;

    if ((opChar == 'a') || (opChar == 'A'))
        operation = ADD;

    else if ((opChar == 's') || (opChar == 'S'))
        operation = SUBTRACT;

    else if ((opChar == 'm') || (opChar == 'M'))
        operation = MULTIPLY;

    else if ((opChar == 'd') || (opChar == 'D'))
        operation = DIVIDE;

    else if ((opChar == 'q') || (opChar == 'Q'))
        operation = QUIT;

    else
        operation = ERROR;

    
    if (operation != QUIT)
        cin >> cmplx1.real >> cmplx1.imag >> cmplx2.real >> cmplx2.imag;


    return operation;
}


void welcomeMessage (void)
{
    cerr << "Complex Calculator\n\n";
    cerr << " Type a letter to specify the arithmetic operator (A, S, M, D)\n"
        << " followed by two complex numbers expressed as pairs of "
        << "doubles.\n Type Q to quit.\n\n";

    return;
}


Complex cmplxMath (short operation, Complex cmplx1, Complex cmplx2)
{
    if (operation == ADD)
        return cmplxAdd (cmplx1, cmplx2);

    else if (operation == SUBTRACT)
        return cmplxSubtract (cmplx1, cmplx2);

    else if (operation == MULTIPLY)
        return cmplxMultiply (cmplx1, cmplx2);

    else if (operation == DIVIDE)
        return cmplxDivide (cmplx1, cmplx2);

    else
    {
        Complex error;
        error.real = 0;
        error.imag = 0;

        return error;
    }
}

void handleExpression (short operation, Complex cmplx1, Complex cmplx2)
{
    if ((operation >= ADD) && (operation <= DIVIDE))
        cmplxPrint (cmplxMath(operation, cmplx1, cmplx2));

    else if (operation != QUIT)
        cerr << "Invalid command.  Please try again.\n";

    return;
}
