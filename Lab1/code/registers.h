#ifndef REGISTERS_H
#define REGISTERS_H
#include "util.h"



#define PERIPH_BASE           ((uint32_t)0x40000000)
#define AHBPERIPH_BASE        (PERIPH_BASE + 0x20000)
#define RCC_BASE              (AHBPERIPH_BASE + 0x1000)
#define RCC_APB2ENR           (RCC_BASE + 0x18)
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000)
#define GPIOB_BASE            (APB2PERIPH_BASE + 0x0C00)
#define GPIOB_ODR             (GPIOB_BASE + 0x0C)
#define GPIOB_CRH             (GPIOB_BASE + 0x04)
#define GPIOB_BSRR            (GPIOB_BASE  + 0x10)
#define GPIOB_BRR             (GPIOB_BASE  + 0x14)
#define GPIOB_OFF             ((uint32_t)0x0000FF00)
#define OUT_TYPE              ((uint32_t)0x33333333)
#define PB8_ON                ((uint32_t)0x00000100)      
#define PB8_OFF               ((uint32_t)0x01000000) 
#define PB9_ON                ((uint32_t)0x00000200)  
#define PB9_OFF               ((uint32_t)0x02000000)    

void setupRegs(void);

#endif      
