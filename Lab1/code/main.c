#include <stdio.h>
#include <stdint.h>
#include "main.h"




int main()
{
	int i;
	uint32_t Mask;

	setupRegs();
	//Need to setup the the LED's on the board
  * regRCC_APB2ENR |= RCC_APB2ENR; // Enable Port B clock
	* regGPIOB_ODR  &= ~GPIOB_OFF;  // switch off outputs that are not 8-15 port B
	* regGPIOB_CRH  = OUT_TYPE; //50MHz  General Purpose output push-pull (open-drain)
  
	while (1)
	{
		Mask = 0x01000200;
		for( i = 0; i < 7; i++) 
		{
			* regGPIOB_BSRR = Mask;
			Mask = Mask << 1;
			delay(600000);
		}
		Mask = 0x80004000;
		for( i = 0; i < 7; i++) 
		{
			* regGPIOB_BSRR = Mask;
			Mask = Mask >> 1;
			delay(600000);
		}
	}
  
}


void delay (uint32_t cycles)
{
	uint32_t i;
	for (i = 0; i < cycles; i++)
	{
		
	}
	
	return;
}

