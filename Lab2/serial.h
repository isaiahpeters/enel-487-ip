#include <stdint.h>
#include <stdbool.h>
#include "registers.h"

#define APB2ENR_GPIOA            ((uint32_t)0x00000004)
#define USART_ENABLE             ((uint32_t)0x00020000)
#define USART_SR_TXE             ((uint32_t)0x00000080)
#define USART_DR_DR              ((uint32_t)0x000001FF)
#define USART_SR_RXNE            ((uint32_t)0x00000020)

// Setting registers for Usart, but not intializing it
void openUsart (void);

// Resets Usart registers to intial values
void closeUsart (void);

// Initializes Usart to be capable of sending/receving
void startUsart (void);

// Stops Usart from being capable of sending/receving
void stopUsart (void);

// Send a char via USART
void sendByte (char);

// Recieve a char via USART
char getByte (void);
