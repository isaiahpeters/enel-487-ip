#ifndef REGISTERS_H
#define REGISTERS_H
#include <stdint.h>



#define PERIPH_BASE           ((uint32_t)0x40000000)

#define AHBPERIPH_BASE        (PERIPH_BASE + 0x20000)
#define APB1PERIPH_BASE       (PERIPH_BASE)
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000)
#define RCC_BASE              (AHBPERIPH_BASE + 0x1000)
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000)
#define GPIOB_BASE            (APB2PERIPH_BASE + 0x0C00)
#define GPIOA_BASE            (APB2PERIPH_BASE + 0x0800)
#define USART2_BASE           (APB1PERIPH_BASE + 0x4400)


#define RCC_APB1ENR           (* (uint32_t volatile *) (RCC_BASE + 0x1C))
#define RCC_APB2ENR           (* (uint32_t volatile *) (RCC_BASE + 0x18))
#define GPIOB_ODR             (* (uint32_t volatile *) (GPIOB_BASE + 0x0C))
#define GPIOB_CRH             (* (uint32_t volatile *) (GPIOB_BASE + 0x04))
#define GPIOB_BSRR            (* (uint32_t volatile *) (GPIOB_BASE  + 0x10))
#define GPIOB_BRR             (* (uint32_t volatile *) (GPIOB_BASE  + 0x14))


#define GPIOA_CRL             (* (uint32_t volatile *) (GPIOA_BASE))
#define USART2_BRR            (* (uint32_t volatile *) (USART2_BASE + 0x08))
#define USART2_CR1            (* (uint32_t volatile *) (USART2_BASE + 0x0C))
#define USART2_CR2            (* (uint32_t volatile *) (USART2_BASE + 0x10))
#define USART2_CR3            (* (uint32_t volatile *) (USART2_BASE + 0x14))
#define USART2_SR             (* (uint32_t volatile *) (USART2_BASE + 0x00))
#define USART2_DR             (* (uint32_t volatile *) (USART2_BASE + 0x04))




#endif      
