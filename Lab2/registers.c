
#include <stdint.h>
#include "registers.h"

volatile uint32_t * regRCC_APB2ENR;
volatile uint32_t * regGPIOB_ODR;
volatile uint32_t * regGPIOB_CRH;
volatile uint32_t * regGPIOB_BSRR;
volatile uint32_t * regGPIOB_BRR;
volatile uint32_t * regGPIOA_BASE;
volatile uint32_t * regGPIOA_CRL;
volatile uint32_t * regRCC_APB1ENR_USART2;


void setupRegs(void)
{
	regRCC_APB2ENR = (volatile uint32_t *)RCC_APB2ENR;
  regGPIOB_ODR =  (volatile uint32_t *)GPIOB_ODR ; 
	regGPIOB_CRH =  (volatile uint32_t *)GPIOB_CRH ; 
	regGPIOB_BSRR =  (volatile uint32_t *)GPIOB_BSRR ; 
  regGPIOB_BRR =  (volatile uint32_t *)GPIOB_BRR ; 
	regGPIOA_BASE = (volatile uint32_t *)GPIOA_BASE; 
	regGPIOA_CRL = (volatile uint32_t *)GPIOA_CRL; 
	regRCC_APB1ENR_USART2 = (volatile uint32_t *)APB1PERIPH_BASE;
}
