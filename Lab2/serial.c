#include <stdint.h>
#include "serial.h"
#include "registers.h"



// Setting registers for Usart, but not intializing it
void openUsart (void)
{
	RCC_APB2ENR |= APB2ENR_GPIOA; // Enable clock for GPIOA, set bit 3
	GPIOA_CRL &= ~0x0000FF00;
	GPIOA_CRL  |= 0x00004B00; // Setup PA2 as Alternate function outupt 50 MHz, PA3 as Floating input
	RCC_APB1ENR |= USART_ENABLE;
	
	USART2_BRR = 0xDF20; // Set clock divider to 3570
	/**
	Set bit 13: Enable USART
	Set bit 2: Enable Recieve
	Set bit 3: Enable Transmit

	Clear bit 12: Sets word length (1 Start bit, 8 Data bits, n Stop bit)
	Clear bit 10: Disables parity control
	
	*/
	USART2_CR1 |= 0x200C; 
	USART2_CR1 &= ~0x1400;
}

// Resets Usart registers to intial values
void closeUsart (void)
{
	
}

// Initializes Usart to be capable of sending/receving
void startUsart (void);

// Stops Usart from being capable of sending/receving
void stopUsart (void);

void sendByte (char bit)
{
	while ((USART2_SR & USART_SR_TXE) != USART_SR_TXE)
		{
		}
		USART2_DR = bit;
}

char getByte (void)
{
	char bit;
	
	if((USART2_SR & USART_SR_RXNE) == USART_SR_RXNE)
	{
		bit = USART2_DR & USART_DR_DR;
	}
	else
	{
		bit = 0x00;
	}
	
	return bit;
}