
#include <stdint.h>
#include "registers.h"
#include "ledControl.h"


void ledOn (uint32_t led)
{
	switch (led)
	{
		case 8: 
			GPIOB_BSRR = PB8_ON;
			break;
		
		case 9:
			GPIOB_BSRR = PB9_ON;
			break;
		
		case 10:
			GPIOB_BSRR = PB10_ON;
			break;
		
		case 11:
			GPIOB_BSRR = PB11_ON;
			break;
		
		case 12:
			GPIOB_BSRR = PB12_ON;
			break;
		
		case 13:
			GPIOB_BSRR = PB13_ON;
			break;
		
		case 14:
			GPIOB_BSRR = PB14_ON;
			break;
		
		case 15:
			GPIOB_BSRR = PB15_ON;
			break;
		
		default: 
			break;
	}
}

void ledOff (uint32_t led) 
{
	switch (led)
	{
		case 8: 
			GPIOB_BSRR = PB8_OFF;
			break;
		
		case 9:
			GPIOB_BSRR = PB9_OFF;
			break;
		
		case 10:
			GPIOB_BSRR = PB10_OFF;
			break;
		
		case 11:
			GPIOB_BSRR = PB11_OFF;
			break;
		
		case 12:
			GPIOB_BSRR = PB12_OFF;
			break;
		
		case 13:
			GPIOB_BSRR = PB13_OFF;
			break;
		
		case 14:
			GPIOB_BSRR = PB14_OFF;
			break;
		
		case 15:
			GPIOB_BSRR = PB15_OFF;
			break;
		
		default:
			break;
	}
}
	


void allLedOn (void) 
{
	GPIOB_BSRR = PBALL_ON;
}

void allLedOff (void) 
{
	GPIOB_BSRR = PBALL_OFF;
}


int queryLed (uint32_t);
