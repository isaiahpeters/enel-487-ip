#include <stdint.h>
#include <stdbool.h>


#define GPIOB_OFF             ((uint32_t)0x0000FF00)
#define OUT_TYPE              ((uint32_t)0x33333333)
#define PB8_ON                ((uint32_t)0x00000100)
#define PB8_OFF               ((uint32_t)0x01000000)
#define PB9_ON                ((uint32_t)0x00000200)
#define PB9_OFF               ((uint32_t)0x02000000)
#define PB10_ON               ((uint32_t)0x00000400)
#define PB10_OFF              ((uint32_t)0x04000000)
#define PB11_ON               ((uint32_t)0x00000800)
#define PB11_OFF              ((uint32_t)0x08000000)
#define PB12_ON               ((uint32_t)0x00001000)
#define PB12_OFF              ((uint32_t)0x10000000)
#define PB13_ON               ((uint32_t)0x00002000)
#define PB13_OFF              ((uint32_t)0x20000000)
#define PB14_ON               ((uint32_t)0x00004000)
#define PB14_OFF              ((uint32_t)0x40000000)
#define PB15_ON               ((uint32_t)0x00008000)
#define PB15_OFF              ((uint32_t)0x80000000)
#define PBALL_ON              ((uint32_t)0x0000ff00)
#define PBALL_OFF             ((uint32_t)0xff000000)

void ledOn (uint32_t);

void ledOff (uint32_t);


void allLedOn (void);

void allLedOff (void);


int queryLed (uint32_t);
